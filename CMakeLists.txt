# Experimental CMake file for Mitusba
# Tested only on Windows and Linux (Ubuntu 10.10)
cmake_minimum_required(VERSION 2.8.3 FATAL_ERROR)

# Internal variable to know whether this is the first time CMake runs
if (NOT DEFINED MTS_CMAKE_INIT)
  set(MTS_CMAKE_INIT ON CACHE INTERNAL "Is this the initial CMake run?")
else()
  set(MTS_CMAKE_INIT OFF CACHE INTERNAL "Is this the initial CMake run?")
endif()

# Allow to override the default project name "mitsuba"
if (NOT DEFINED MTS_PROJECT_NAME)
  set(MTS_PROJECT_NAME "mitsuba")
endif()
project(${MTS_PROJECT_NAME})

# Tell cmake where to find the additional modules
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/data/cmake")
# Make sure the cmake-provided modules use the versions they expect
if(NOT CMAKE_VERSION VERSION_LESS "2.8.4")
  cmake_policy(SET CMP0017 NEW)
endif()

# Enable folders for projects in Visual Studio
if (CMAKE_GENERATOR MATCHES "Visual Studio")
  set_property(GLOBAL PROPERTY USE_FOLDERS ON)
endif()

# Remove Debug from CMAKE_CONFIGURATION_TYPES as the dependencies do not contain the necessary debug libraries
if(MSVC AND CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_CONFIGURATION_TYPES Release MinSizeRel RelWithDebInfo)
  set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING
    "Remove Debug from available configuration types"
     FORCE)
endif()


# make config 'RelWithDebInfo' debuggable by disabling optimizations
if(MSVC)
  # /MD link against release libs, /Od disable optimizations, /Ob0
  if(CMAKE_C_FLAGS_RELWITHDEBINFO MATCHES "/O2")
    string(REGEX REPLACE "/O2" "/Od" CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO}")
  endif()
  if(CMAKE_CXX_FLAGS_RELWITHDEBINFO MATCHES "/O2")
    string(REGEX REPLACE "/O2" "/Od" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
  endif()
  if(CMAKE_C_FLAGS_RELWITHDEBINFO MATCHES "/Ob1")
    string(REGEX REPLACE "/Ob1" "/Ob0" CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO}")
  endif()
  if(CMAKE_CXX_FLAGS_RELWITHDEBINFO MATCHES "/Ob1")
    string(REGEX REPLACE "/Ob1" "/Ob0" CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
  endif()
  set(CMAKE_C_FLAGS_RELWITHDEBINFO "${CMAKE_C_FLAGS_RELWITHDEBINFO} /RTC1")
  set(CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} /RTC1")
  list(REMOVE_DUPLICATES CMAKE_C_FLAGS_RELWITHDEBINFO)
  list(REMOVE_DUPLICATES CMAKE_CXX_FLAGS_RELWITHDEBINFO)
endif()

# Set CMAKE_BUILD_TYPE to Release by default
if (MTS_CMAKE_INIT AND DEFINED CMAKE_BUILD_TYPE AND NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release" CACHE STRING
    "Choose the type of build, options are: Debug, Release, RelWithDebInfo, MinSizeRel." FORCE)
endif()

# Some experimentation to add clang support directly
############################################################################
## OpenEXR
##SET(ILMBASE_NAMESPACE_VERSIONING OFF CACHE BOOL " " FORCE)
##SET(OPENEXR_NAMESPACE_VERSIONING OFF CACHE BOOL " " FORCE)
#
## This is the default behavior in original code in PBRT
##SET(ILMBASE_BUILD_SHARED_LIBS    OFF CACHE BOOL " " FORCE)
##SET(OPENEXR_BUILD_SHARED_LIBS	 OFF CACHE BOOL " " FORCE)
#
## This lines was for pybind fix
##IF(WIN32)
##  SET(ILMBASE_BUILD_SHARED_LIBS    OFF CACHE BOOL " " FORCE)
##  SET(OPENEXR_BUILD_SHARED_LIBS	 OFF CACHE BOOL " " FORCE)
##ELSE()
#SET(PYILMBASE_ENABLE OFF CACHE BOOL " " FORCE)
#SET(OPENEXR_VIEWERS_ENABLE OFF CACHE BOOL " " FORCE)
#SET(OPENEXR_BUILD_UTILS OFF CACHE BOOL " " FORCE)
#SET(ILMBASE_BUILD_SHARED_LIBS    ON CACHE BOOL " " FORCE)
#SET(OPENEXR_BUILD_SHARED_LIBS	 ON CACHE BOOL " " FORCE)
##ENDIF()
#ADD_SUBDIRECTORY(ext/openexr)
#
#SET_PROPERTY(TARGET IexMath eLut toFloat b44ExpLogTable dwaLookups IlmThread Imath IlmImf PROPERTY FOLDER "ext")
#
#INCLUDE_DIRECTORIES (
#        ext/openexr/IlmBase/Imath
#        ext/openexr/IlmBase/Half
#        ext/openexr/IlmBase/Iex
#        ext/openexr/OpenEXR/IlmImf
#        ${CMAKE_BINARY_DIR}/ext/openexr/IlmBase/config
#        ${CMAKE_BINARY_DIR}/ext/openexr/OpenEXR/config
#)
#IF(WIN32)
#  SET(OPENEXR_LIBRARIES IlmImf Imath Half ${ZLIB_LIBRARY})
#ELSE()
#  SET(OPENEXR_LIBRARIES IlmImf Imath Half)
#ENDIF()
## As we compile OpenEXR we activate the support by default
#add_definitions(-DMTS_HAS_OPENEXR=1)


# Load the required modules
include (MitsubaUtil)
include (MtsGetVersionInfo)
include (CheckCXXSourceCompiles)
include (CMakeDependentOption)

# Read the version information
MTS_GET_VERSION_INFO()
if (MTS_HAS_VALID_REV)
  message(STATUS "mitsuba ${MTS_VERSION}-hg${MTS_REV_ID} (${MTS_DATE})")
else()
  message(STATUS "mitsuba ${MTS_VERSION} (${MTS_DATE})")
endif()

# Setup sanitiser for GCC
#set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -fno-omit-frame-pointer -fsanitize=address -fsanitize=undefined")
#set (CMAKE_LINKER_FLAGS_DEBUG "${CMAKE_LINKER_FLAGS_DEBUG} -fno-omit-frame-pointer -fsanitize=address -fsanitize=undefined")

# If we want to use the naive product computation
# Note that this option do not anything if enoki is used
option(MTS_USE_SLOW_PROD "Use naive LTC product" OFF)
if (MTS_USE_SLOW_PROD)
  add_definitions(-DMTS_USE_SLOW_PROD)
endif()

###########################################################################
# Enoki
# Make optional to use Enoki
option(MTS_USE_ENOKI "Enable Enoki support for path guiding" ON)
if (MTS_USE_ENOKI)
  add_definitions(-DMTS_USE_ENOKI)
endif()

#if (MTS_USE_ENOKI)
  INCLUDE (CheckCXXSourceCompiles)
  INCLUDE (CheckCXXSourceRuns)
  macro(CHECK_CXX_COMPILER_AND_LINKER_FLAGS _RESULT _CXX_FLAGS _LINKER_FLAGS)
    set(CMAKE_REQUIRED_FLAGS ${_CXX_FLAGS})
    set(CMAKE_REQUIRED_LIBRARIES ${_LINKER_FLAGS})
    set(CMAKE_REQUIRED_QUIET TRUE)
    check_cxx_source_runs("#include <iostream>\nint main(int argc, char **argv) { std::cout << \"test\"; return 0; }" ${_RESULT})
    set(CMAKE_REQUIRED_FLAGS "")
    set(CMAKE_REQUIRED_LIBRARIES "")
  endmacro()

  INCLUDE_DIRECTORIES (
          dependencies/ext/enoki/include
          ${CMAKE_BINARY_DIR}/dependencies/enoki/include
  )
  # For Linux, it seems that -march is not detected properly
  #ADD_DEFINITIONS ( -march=native -fno-stack-protector -fomit-frame-pointer -fno-math-errno -O3)
  if (MSVC)
    set(ENOKI_ARCHFLAGS_FILE archflags_win32.cpp)
    if (CMAKE_SIZEOF_VOID_P EQUAL 4)
      message(WARNING "Enoki does not support vectorization on 32-bit Windows due to various")
      message(WARNING "platform limitations (unaligned stack, calling conventions don't allow")
      message(WARNING "passing vector registers, etc.). Switching to scalar mode.")
    endif()
  else()
    set(ENOKI_ARCHFLAGS_FILE archflags_unix.cpp)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native")
  endif()

  if (${CMAKE_SYSTEM_PROCESSOR} MATCHES "armv7")
    set(ENOKI_ARCHFLAGS -march=armv7-a -mtune=cortex-a7 -mfpu=neon-vfpv4 -mfloat-abi=hard -mfp16-format=ieee)
    set(ENOKI_NATIVE_FLAGS ${ENOKI_ARCHFLAGS})
  elseif (${CMAKE_SYSTEM_PROCESSOR} MATCHES "aarch64")
    set(ENOKI_ARCHFLAGS -march=armv8-a+simd -mtune=cortex-a53)
    set(ENOKI_NATIVE_FLAGS ${ENOKI_ARCHFLAGS})
  elseif (CMAKE_CXX_COMPILER MATCHES "/em\\+\\+(-[a-zA-Z0-9.])?$")
    # Emscripten
  else()
    if (UNIX)
      set(ENOKI_ARCHFLAGS_LINK_LIBRARIES ${CMAKE_EXE_LINKER_FLAGS})
    endif()
    try_run(
            ENOKI_ARCHFLAGS_RETVAL ENOKI_ARCHFLAGS_COMPILE_RESULT
            ${CMAKE_BINARY_DIR}
            ${CMAKE_CURRENT_SOURCE_DIR}/data/${ENOKI_ARCHFLAGS_FILE}
            COMPILE_OUTPUT_VARIABLE ENOKI_ARCHFLAGS_MSG
            RUN_OUTPUT_VARIABLE ENOKI_ARCHFLAGS
            LINK_LIBRARIES ${ENOKI_ARCHFLAGS_LINK_LIBRARIES}
    )

    if (NOT ${ENOKI_ARCHFLAGS_COMPILE_RESULT})
      message(FATAL_ERROR "Failed to compile 'archflags' binary: ${ENOKI_ARCHFLAGS_MSG}")
    endif()
  endif()

  set(CMAKE_CXX_FLAGS ${ENOKI_CXXFLAGS_BACKUP})

  # Optimize for current architecture
  if (CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
          CMAKE_CXX_COMPILER_ID MATCHES "GNU")
    if (NOT ENOKI_NATIVE_FLAGS)
      set(ENOKI_NATIVE_FLAGS -march=native)
    endif()
  elseif (CMAKE_CXX_COMPILER_ID MATCHES "Intel")
    set(ENOKI_NATIVE_FLAGS -xHost)
  elseif (MSVC)
    # MSVC doesn't have a flag equivalent to -march=native
    # The 'archflags' executable provides this functionality
    set(ENOKI_NATIVE_FLAGS ${ENOKI_ARCHFLAGS})
  endif()

  set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/resources")

  macro(enoki_set_native_flags)
    add_compile_options(${ENOKI_NATIVE_FLAGS})
    string(REPLACE ";" " " ENOKI_NATIVE_FLAGS_STR "${ENOKI_NATIVE_FLAGS}")
    # Some linkers want to know the architecture flags (for LTO)
    if (NOT MSVC)
      set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${ENOKI_NATIVE_FLAGS_STR}")
      set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} ${ENOKI_NATIVE_FLAGS_STR}")
    endif()
  endmacro()

  macro(enoki_set_compile_flags)
    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang" OR
            CMAKE_CXX_COMPILER_ID MATCHES "GNU" OR
            CMAKE_CXX_COMPILER_ID MATCHES "Intel")
      string(TOUPPER "${CMAKE_BUILD_TYPE}" ENOKI_U_CMAKE_BUILD_TYPE)
      if (NOT (${ENOKI_U_CMAKE_BUILD_TYPE} MATCHES "DEB"))
        if (NOT (${CMAKE_CXX_FLAGS} MATCHES "fsanitize"))
          # Don't use stack security features in release mode
          add_compile_options(-fno-stack-protector)

          # In release mode, don't keep the frame pointer in a dedicated register unless needed
          add_compile_options(-fomit-frame-pointer)
        endif()
      endif()

      # Never update the 'errno' variable due to arithmetic exceptions
      add_compile_options(-fno-math-errno)

      if (NOT CMAKE_CXX_COMPILER_ID MATCHES "Intel")
        # Look for opportunities to fuse additions & multiplications into FMA operations
        add_compile_options(-ffp-contract=fast)
      endif()
    endif()

    # Disable overly aggressive FP optimization in the Intel compiler
    if (CMAKE_CXX_COMPILER_ID MATCHES "Intel")
      add_compile_options(-fp-model precise)
    endif()

    if (MSVC)
      # Disable buffer security check cookie
      set(Configurations RELEASE RELWITHDEBINFO MINSIZEREL)
      foreach(Configuration ${Configurations})
        string(REPLACE "/GS"  "" CMAKE_CXX_FLAGS_${Configuration} "${CMAKE_CXX_FLAGS_${Configuration}}")
      endforeach()
      string(REPLACE "/GS"  "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
      add_compile_options("$<$<NOT:$<CONFIG:Debug>>:/GS->")

      # Enable intrinsic functions
      add_compile_options("$<$<CONFIG:Release>:/Oi>")

      # Honor __forceinline statements even in debug mode, needed to avoid internal compiler errors on MSVC
      string(REPLACE "/Ob0" "" CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG}")
      add_compile_options("$<$<CONFIG:Debug>:/Ob1>")
    endif()
  endmacro()

  # Prefer libc++ in conjunction with Clang
  if (CMAKE_CXX_COMPILER_ID MATCHES "Clang" AND NOT CMAKE_CXX_FLAGS MATCHES "-stdlib=libc\\+\\+")
    CHECK_CXX_COMPILER_AND_LINKER_FLAGS(HAS_LIBCPP "-stdlib=libc++" "-stdlib=libc++")
    if (HAS_LIBCPP)
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++ -D_LIBCPP_VERSION")
      set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -stdlib=libc++")
      set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -stdlib=libc++")
      message(STATUS "Enoki: using libc++.")
    else()
      CHECK_CXX_COMPILER_AND_LINKER_FLAGS(HAS_LIBCPP_AND_CPPABI "-stdlib=libc++" "-stdlib=libc++ -lc++abi")
      if (HAS_LIBCPP_AND_CPPABI)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++ -D_LIBCPP_VERSION")
        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -stdlib=libc++ -lc++abi")
        set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -stdlib=libc++ -lc++abi")
        message(STATUS "Enoki: using libc++ and libc++abi.")
      else()
        message(FATAL_ERROR "When Clang is used to compile Enoki, libc++ must be available -- GCC's libstdc++ is not supported! (please insteall the libc++ development headers, provided e.g. by the packages 'libc++-dev' and 'libc++abi-dev' on Debian/Ubuntu).")
      endif()
    endif()
  endif()

  # Setup all enoki performance compilations
  # These macro call are responsible to enoki
  # optimzation. Comment them out if necessary
  enoki_set_native_flags()
  enoki_set_compile_flags()
  INCLUDE_DIRECTORIES("${CMAKE_CURRENT_SOURCE_DIR}/dependencies/enoki/include")
#endif() # USE_ENOKI

# These are the compilation flags from Scons config file
#add_compile_options("-ftree-vectorize")
#add_compile_options("-mfpmath=sse")
#add_compile_options("-funsafe-math-optimizations")
#add_compile_options("-fno-rounding-math")
#add_compile_options("-fno-signaling-nans")
#add_compile_options("-fno-math-errno")
#add_compile_options("-fomit-frame-pointer")

# For CMAKE version, we force to use updated GLEW
# This remove some code from GLEWmx
add_definitions(-DMTS_NEW_GLEW_VERSION)

# Setup the build options
include (MitsubaBuildOptions)

# Find the external libraries and setup the paths
include (MitsubaExternal)

# Main mitsuba include directory
include_directories("include")

# ===== Prerequisite resources =====

# Process the XML schemas
add_subdirectory(data/schema)
# Add the IOR database
add_subdirectory(data/ior)
# Microfacet precomputed data
add_subdirectory(data/microfacet)
# LTC data
add_subdirectory(data/ltc)


# ===== Build the support libraries ====

# Core support library
add_subdirectory(src/libcore)
# Rendering-related APIs
add_subdirectory(src/librender)
# Hardware acceleration
add_subdirectory(src/libhw)
# Bidirectional support library
add_subdirectory(src/libbidir)
# Python binding library
set(BUILD_PYTHON FALSE)
if (BUILD_PYTHON)
  add_subdirectory(src/libpython)
elseif(NOT PYTHON_FOUND)
  message(STATUS "Python was not found. The bindings will not be built.")
endif()


# Additional files to add to main executables
if(APPLE)
  set(MTS_DARWIN_STUB "${CMAKE_CURRENT_SOURCE_DIR}/src/mitsuba/darwin_stub.mm")
  set(MTS_WINDOWS_STUB "")
elseif(WIN32)
  set(MTS_DARWIN_STUB "")
  set(MTS_WINDOWS_STUB "${CMAKE_CURRENT_SOURCE_DIR}/data/windows/wmain_stub.cpp")
else()
  set(MTS_DARWIN_STUB "")
  set(MTS_WINDOWS_STUB "")
endif()


# ===== Build the applications =====

# Build the command-line binaries
add_subdirectory(src/mitsuba)

# Build the COLLADA converter
if (COLLADA_FOUND)
  add_subdirectory(src/converter)
else()
  message(STATUS "Collada DOM was not found. The importer will not be built.")
endif()

# Build the Qt-based GUI binaries
if (BUILD_GUI)
  add_subdirectory(src/mtsgui)
elseif(NOT QT4_FOUND)
  message(STATUS "Qt4 was not found. The mitsuba gui will not be built.")
endif()


# ===== Build the plugins =====

# Utilities
add_subdirectory(src/utils)
# Surface scattering models
add_subdirectory(src/bsdfs)
# Phase functions
add_subdirectory(src/phase)
# Intersection shapes
add_subdirectory(src/shapes)
# Sample generators
add_subdirectory(src/samplers)
# Reconstruction filters
add_subdirectory(src/rfilters)
# Film implementations
add_subdirectory(src/films)
# Sensors
add_subdirectory(src/sensors)
# Emitters
add_subdirectory(src/emitters)
# Participating media
add_subdirectory(src/medium)
# Volumetric data sources
add_subdirectory(src/volume)
# Sub-surface integrators
add_subdirectory(src/subsurface)
# Texture types
add_subdirectory(src/textures)
# Integrators
add_subdirectory(src/integrators)
# Testcases
add_subdirectory(src/tests)


# ===== Packaging =====

# Use a subdirectory to enforce that packaging runs after all other targets
add_subdirectory(data/cmake/packaging)
