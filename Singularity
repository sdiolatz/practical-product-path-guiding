# Copyright (c) 2015-2016, Maciej Sieczka, Gregory M. Kurtzer. All rights
# reserved.
#
# Copyright (c) 2017-2018, SyLabs, Inc. All rights reserved.
# Copyright (c) 2017, SingularityWare, LLC. All rights reserved.
#
# Copyright (c) 2015-2017, Gregory M. Kurtzer. All rights reserved.
#
# Minimal installation process is defined in
# libexec/bootstrap/modules-v2/dist-arch.sh. A couple extra actions are called
# from here in `%post' section. Adjust them as needed.
# https://wiki.archlinux.org/index.php/Installation_Guide may come in handy.

Bootstrap: docker
From: archlinux/base

%runscript
    echo "This is what happens when you run the container..."

%files

%environment
    #export LC_ALL=C

%labels
    AUTHOR adrien.gruson@gmail.com


%post
    echo "Hello from inside the container"

    # Set time zone. Use whatever you prefer instead of UTC.
    # ln -s /usr/share/zoneinfo/UTC /etc/localtime

    # Set locale. Use whatever you prefer instead of en_US.
    echo 'en_US.UTF-8 UTF-8' > /etc/locale.gen
    locale-gen
    echo 'LANG=en_US.UTF-8' > /etc/locale.conf
    # Mind that Singularity's shell will use host's locale no matter what
    # anyway, as of version 2.1.2. This may change in a future release.

    # Set the package mirror server(s). This is only for the output image's
    # mirrorlist. `pacstrap' can only use your hosts's package mirrors.
    echo 'Server = http://arch.mirror.constant.com/$repo/os/$arch' > /etc/pacman.d/mirrorlist
    # Add any number of fail-over servers, eg:
    echo 'Server = http://mirror.sfo12.us.leaseweb.net/archlinux/$repo/os/$arch' >> /etc/pacman.d/mirrorlist

    # This is the list of common package between GCC and clang build
    pacman -Sy --noconfirm emacs-nox bash-completion cmake fftw libpng jasper zlib xerces-c xorg glew git awk python3 make python-pip eigen scons openmp google-glog freeglut
    # We need to install base-devel for GCC or clang (contain gcc)
    pacman -Sy --noconfirm base-devel 
 
    # This are the package only for GCC. 
    # Note that we could use repository package here as all are compiled with stdc++
    # For clang, we might need ot be a bit more concervative
    pacman -Sy --noconfirm boost openexr

    # For clang, it is a bit more complicated as we need to use AUR :(
    pacman -Sy --noconfirm clang
    # TODO: Check if we want to install the trunk https://aur.archlinux.org/packages/clang-trunk/
    
    # Here it is a bit painful as makepkg and AUR cannot be used by root
    # However, it is the default way to build the package. The solution is to create a fake user.
    pacman -S --needed --noconfirm sudo # Install sudo
    useradd builduser -m # Create the builduser
    passwd -d builduser # Delete the buildusers password
    printf 'builduser ALL=(ALL) ALL\n' | tee -a /etc/sudoers # Allow the builduser passwordless sudo

    # Revert GLIB version
    pacman -U https://archive.archlinux.org/packages/g/glibc/glibc-2.30-3-x86_64.pkg.tar.xz https://archive.archlinux.org/packages/l/lib32-glibc/lib32-glibc-2.30-3-x86_64.pkg.tar.xz

    # Use yay to install libc++ (precompiled)
    wget http://beltegeuse.s3-website-ap-northeast-1.amazonaws.com/libc++.tgz
    tar -xzvf libc++.tgz
    cd libc++
    pacman -Sy --noconfirm ninja clang
    pacman -U --noconfirm *.pkg.tar.xz
    cd ..
    rm -frv libc++
    rm libc++.tgz
    cd

    # Install LLVM-11 (GIT version), precompiled
    # TODO: Not used due to Enoki bug :(
    #wget http://beltegeuse.s3-website-ap-northeast-1.amazonaws.com/llvm-git.tgz
    #tar -xzvf llvm-git.tgz
    #rm llvm-git.tgz
    #cd llvm-git
    #yes |  pacman -U *.pkg.tar.xz
    #cd ..
    #rm -frv llvm-git
    #cd

    # Use yay to install cerces solver (custom version)
    sudo -u builduser bash -c 'cd;git clone https://github.com/beltegeuse/ceres-solver-aur.git;cd ceres-solver-aur'
    pacman -Sy --noconfirm suitesparse eigen
    sudo -u builduser bash -c 'cd;cd ceres-solver-aur;makepkg -s --noconfirm --skippgpcheck'
    cd /home/builduser/ceres-solver-aur
    pacman -U --noconfirm *.pkg.tar.xz
    cd ..
    rm -frv ceres-solver-aur
    cd
    
    # TODO: At the end we want to delete builduser to not espace the container
    #    This is for obious security reasons
    

    # Other modules (python). 
    # Usefull if we want to generate webpage using https://github.com/joeylitalien/interactive-viewer
    #pip install --no-cache-dir opencv-python
    #pip install --no-cache-dir pyexr
    #pip install --no-cache-dir scikit-image
    #pip install --no-cache-dir seaborn
    #pip install --no-cache-dir matplotlib
    #pip install --no-cache-dir Pillow
    #pip install --no-cache-dir html5lib
    #pip install --no-cache-dir bs4

    # Remove the packages downloaded to image's Pacman cache dir.
    pacman -Sy --noconfirm pacman-contrib
    paccache -r -k0
