Practical Product Path Guiding Using Linearly Transformed Cosines
===================================

![Teaser](/uploads/f58aa8b579ee7f414d07de6e0ac8c3e5/Capture.PNG)

Authors' implementation of [Practical Product Path Guiding Using Linearly Transformed Cosines [DGJND20]](http://www-sop.inria.fr/reves/Basilic/2020/DGJND20/) based on the [Mitsuba Physically Based Renderer](http://mitsuba-renderer.org/).

# Contents

- Practical product path guiding integrator (src/integrators/path/product_guided_path) including precomputation and enoki AVX 512 optimizations
- Practical path guiding integrator (Müller et al. 2017) (src/integrators/path/guided_path) with adjoint-based Russian roullette and on-the-fly pdf computation

# Scenes

The Bathroom and Glossy Kitchen scenes can be bought with this [pack](https://evermotion.org/shop/show_product/archinteriors-vol-1/37)

The Living Room scene is provided in [Bitterli's repository](https://benedikt-bitterli.me/resources/)

The Jewelry scene was provided by Sebastian Herholz

The Attic and Pink Kitchen scenes can be bought with this [pack](https://evermotion.org/shop/show_product/archinteriors-vol-30/10469)


# Compilation

You can compile this version of Mitsuba either using scons following the [official documentation](http://mitsuba-renderer.org/docs.html) or by using cmake.
A singularity image is aslo included to set up the environment for compilation.

## Compilation Flags

-MTS_USE_ENOKI: Flag that enables ths enoki optimization for AVX-512 CPUs

-MTS_USE_SLOW_PROD: Use slow product w/o the various optimizations and precomputation

# License

The new code introduced by this project is licensed under the GNU General Public License (Version 3). Please consult the bundled LICENSE file for the full license text.