#pragma once

#include <mitsuba/render/scene.h>

#ifdef MTS_USE_ENOKI
#include "enoki/array.h"
#include "enoki/matrix.h"

// Enoki struct definitions
using FloatP = enoki::Packet<float, 4>;
using FloatPL = enoki::Packet<float, 16>;
using IntegerPL = enoki::Packet<int, 16>;
using EnokiVector3fPL = enoki::Array<FloatPL, 3>;
using EnokiMatrix3f = enoki::Matrix<float, 3>;
static constexpr float Pi = 3.14159265358979323846;

using FloatP4 = enoki::Array<float, 4>;
using Vector3fP4 = enoki::Array<FloatP4, 3>;
using UInt32P4 = enoki::Array<uint32_t, 4>;

#endif

MTS_NAMESPACE_BEGIN

// Size of precomputed table (theta, alpha)
static const int N = 64;
// Number of samples used to compute the error during fitting
static const int Nsample = 32;
// Minimal roughness (avoid singularities)
static const float MIN_ALPHA = 0.00001f;

// Precomputed spheres clipping approximation
#define LUT_CLIPPING_SIZE 64
static float LUT_CLIPPING[LUT_CLIPPING_SIZE * LUT_CLIPPING_SIZE];

// Precomputed invM LTC transformation
#define LUT_TABM_INV_SIZE 128
static float LUT_TABM_INV[9 * LUT_TABM_INV_SIZE * LUT_TABM_INV_SIZE];

// Precomputed diffuse vector form factors
#ifdef MTS_USE_ENOKI
static const int LUT_COSINE_SIZE = 65532;
static float LUT_COSINE[LUT_COSINE_SIZE];
#else
static const int LUT_COSINE_SIZE = 5460;
static Vector LUT_COSINE[LUT_COSINE_SIZE];
#endif

static inline Vector canonicalToDir(float u1, float u2) {
    const Float cosTheta = 2.0f * u1 - 1.0f;
    const Float phi = 2.0f * M_PI * u2;
    const Float sinTheta = sqrt(1.0f - cosTheta * cosTheta);

    return { sinTheta * cosf(phi), sinTheta * sinf(phi), cosTheta };
}

// Avoid singularity on the poles of the spherical representation 
// by slightly moving the vertices
static inline Vector canonicalToDirSing(float u1, float u2) {
    const auto EPS_SING = 0.005f;

    if (u1 < EPS_SING) {
        u1 = EPS_SING;
    }
    else if (u1 > (1.0f - EPS_SING)) {
        u1 = (1.0f - EPS_SING);
    }

    if (u2 < EPS_SING) {
        u2 = EPS_SING;
    }
    else if (u2 > (1.0f - EPS_SING)) {
        u2 = (1.0f - EPS_SING);
    }

    return canonicalToDir(u1, u2);
}

struct LTC {

    // parametric representation
    float invm11, invm22, invm33, invm13, invm31;

    bool diffuse = true;

    Matrix3x3 T;
    Matrix3x3 transposeT;

    Matrix3x3 invM;

#ifdef MTS_USE_ENOKI
    EnokiMatrix3f enokiInvM;
    EnokiMatrix3f enokiTransposeT;
#endif

    LTC() {
        invm11 = 1.0f;
        invm22 = 1.0f;
        invm33 = 1.0f;
        invm13 = 0.0f;
        invm31 = 0.0f;
    }

    //Compute matrix from parameters
    void update() {
        invM = Matrix3x3(invm11,   0.0f, invm13,
                         0.0f, invm22,   0.0f,
                         invm31,   0.0f, invm33);

#ifdef MTS_USE_ENOKI

        enokiInvM = EnokiMatrix3f(
            invM.m[0][0], invM.m[0][1], invM.m[0][2],
            invM.m[1][0], invM.m[1][1], invM.m[1][2],
            invM.m[2][0], invM.m[2][1], invM.m[2][2]
        );

        EnokiMatrix3f enokiT = EnokiMatrix3f(
            T.m[0][0], T.m[0][1], T.m[0][2],
            T.m[1][0], T.m[1][1], T.m[1][2],
            T.m[2][0], T.m[2][1], T.m[2][2]
        );

        // Compute inverses
        enokiTransposeT = transpose(enokiT);

        enokiInvM = enokiInvM * enokiTransposeT;
#else
        // Compute inverses
        T.transpose(transposeT);

        invM = invM * transposeT;
#endif

    }

    float eval(const Vector& L) const {
        Matrix3x3 M;
        if (!invM.invert(M)) {
            SLog(EWarn, "Impossible to inverse matrix");
            return 0.0f;
        }

        float detM = M.det();
        Vector Loriginal = normalize(invM * L);
        Vector L_ = M * Loriginal;
        float l = L_.length();
        float Jacobian = detM / (l*l*l);
        float D = 1.0f / M_PI * std::max(0.0f, Loriginal.z);
        return D / Jacobian;
    }

    Vector sample(Point2f sample) const
    {
        Matrix3x3 M;
        if (!invM.invert(M)) {
            SLog(EWarn, "Impossible to inverse matrix");
            return Vector(0.0f, 0.0f, -1.0f); // Invalid direction
        }
        const float theta = acosf(sqrtf(sample.x));
        const float phi = 2.0f * M_PI * sample.y;
        const Vector L = normalize(M * Vector(sinf(theta)*cosf(phi), sinf(theta)*sinf(phi), cosf(theta)));

        return L;
    }

    void setTransformation(Matrix3x3 &transf) {
        T = transf;

        update();
    }

    void addTransformation(Matrix3x3 &transf) {
        T = transf * T;

        update();
    }

    // Slow clipping of quad to horizon
    static inline void ClipQuadToHorizon(Vector* L, int& n) {
        // Detect clipping config
        int config = 0;
        if (L[0].z > 0.0f) config += 1;
        if (L[1].z > 0.0f) config += 2;
        if (L[2].z > 0.0f) config += 4;
        if (L[3].z > 0.0f) config += 8;

        // Clip
        n = 0;

        if (config == 0) {
            // Clip all
        }
        // V1 clip V2 V3 V4
        else if (config == 1) {
            n = 3;
            L[1] = -L[1].z * L[0] + L[0].z * L[1];
            L[2] = -L[3].z * L[0] + L[0].z * L[3];
        }
        // V2 clip V1 V3 V4
        else if (config == 2) {
            n = 3;
            L[0] = -L[0].z * L[1] + L[1].z * L[0];
            L[2] = -L[2].z * L[1] + L[1].z * L[2];
        }
        // V1 V2 clip V3 V4
        else if (config == 3) {
            n = 4;
            L[2] = -L[2].z * L[1] + L[1].z * L[2];
            L[3] = -L[3].z * L[0] + L[0].z * L[3];
        }
        // V3 clip V1 V2 V4
        else if (config == 4) {
            n = 3;
            L[0] = -L[3].z * L[2] + L[2].z * L[3];
            L[1] = -L[1].z * L[2] + L[2].z * L[1];
        }
        // V1 V3 clip V2 V4) impossible
        else if (config == 5) {
            n = 0;
        }
        // V2 V3 clip V1 V4
        else if (config == 6) {
            n = 4;
            L[0] = -L[0].z * L[1] + L[1].z * L[0];
            L[3] = -L[3].z * L[2] + L[2].z * L[3];
        }
        // V1 V2 V3 clip V4
        else if (config == 7) {
            n = 5;
            L[4] = -L[3].z * L[0] + L[0].z * L[3];
            L[3] = -L[3].z * L[2] + L[2].z * L[3];
        }
        // V4 clip V1 V2 V3
        else if (config == 8) {
            n = 3;
            L[0] = -L[0].z * L[3] + L[3].z * L[0];
            L[1] = -L[2].z * L[3] + L[3].z * L[2];
            L[2] = L[3];
        }
        // V1 V4 clip V2 V3
        else if (config == 9) {
            n = 4;
            L[1] = -L[1].z * L[0] + L[0].z * L[1];
            L[2] = -L[2].z * L[3] + L[3].z * L[2];
        }
        // V2 V4 clip V1 V3) impossible
        else if (config == 10) {
            n = 0;
        }
        // V1 V2 V4 clip V3
        else if (config == 11) {
            n = 5;
            L[4] = L[3];
            L[3] = -L[2].z * L[3] + L[3].z * L[2];
            L[2] = -L[2].z * L[1] + L[1].z * L[2];
        }
        // V3 V4 clip V1 V2
        else if (config == 12) {
            n = 4;
            L[1] = -L[1].z * L[2] + L[2].z * L[1];
            L[0] = -L[0].z * L[3] + L[3].z * L[0];
        }
        // V1 V3 V4 clip V2
        else if (config == 13) {
            n = 5;
            L[4] = L[3];
            L[3] = L[2];
            L[2] = -L[1].z * L[2] + L[2].z * L[1];
            L[1] = -L[1].z * L[0] + L[0].z * L[1];
        }
        // V2 V3 V4 clip V1
        else if (config == 14) {
            n = 5;
            L[4] = -L[0].z * L[3] + L[3].z * L[0];
            L[0] = -L[0].z * L[1] + L[1].z * L[0];
        }
        // V1 V2 V3 V4
        else if (config == 15) {
            n = 4;
        }

        if (n == 3)
            L[3] = L[0];
        if (n == 4)
            L[4] = L[0];
    }

    float integrateQuadTreeEdge(Vector &v1, Vector &v2) const {
        float x = dot(v1, v2);
        float y = std::abs(x);

        // Avoid the use of acos to compute theta by
        // using a fit of the theta over sintheta function (see [HH16])
        float a = 0.8543985f + (0.4965155f + 0.0145206f * y) * y;
        float b = 3.4175940f + (4.1616724f + y) * y;
        float v = a / b;

        float theta_sintheta = (x > 0.0f) ? v : 0.5f * (1.0f / sqrt(std::max(1.0f - x * x, 1e-7f))) - v;

        return cross(v1, v2).z * theta_sintheta;
    }

    float integrateQuadTreeEdgeSlow(Vector& v1, Vector& v2) const {
        float cosTheta = dot(v1, v2);
        float theta = acos(cosTheta);
        float res = cross(v1, v2).z * ((theta > 0.001f) ? theta / sin(theta) : 1.0f);

        return res;
    }

    Vector integrateQuadTreeEdgeVec(Vector &v1, Vector &v2) const {
        //Avoid the use of acos to compute theta by
        //using a fit of the theta over sintheta function
        float x = dot(v1, v2);
        float y = std::abs(x);

        // Avoid the use of acos to compute theta by
        // using a fit of the theta over sintheta function (see [HH16])
        float a = 0.8543985f + (0.4965155f + 0.0145206f * y) * y;
        float b = 3.4175940f + (4.1616724f + y) * y;
        float v = a / b;

        float theta_sintheta = (x > 0.0f) ? v : 0.5f * (1.0f / sqrt(std::max(1.0f - x * x, 1e-7f))) - v;

        return cross(v1, v2) * theta_sintheta;
    }

    inline Vector integrateQuadTreeQuadVec(const Point2f& p, float size) const {
        Vector value(0.0f, 0.0f, 0.0f);

        Vector L[4];
        if (size >= 0.5f) {
            // Handling the singularity
            L[0] = (canonicalToDirSing(p.x + size, p.y));
            L[1] = (canonicalToDirSing(p.x, p.y));
            L[2] = (canonicalToDirSing(p.x, p.y + size));
            L[3] = (canonicalToDirSing(p.x + size, p.y + size));
        }
        else {
            L[0] = (canonicalToDir(p.x + size, p.y));
            L[1] = (canonicalToDir(p.x, p.y));
            L[2] = (canonicalToDir(p.x, p.y + size));
            L[3] = (canonicalToDir(p.x + size, p.y + size));
        }

        L[0] = normalize(L[0]);
        L[1] = normalize(L[1]);
        L[2] = normalize(L[2]);
        L[3] = normalize(L[3]);

        value += integrateQuadTreeEdgeVec(L[0], L[1]);
        value += integrateQuadTreeEdgeVec(L[1], L[2]);
        value += integrateQuadTreeEdgeVec(L[2], L[3]);
        value += integrateQuadTreeEdgeVec(L[3], L[0]);

        return value;
    }

#ifndef MTS_USE_ENOKI
    inline float integrateQuadTreeQuadSlow(const Point2f &p, float size) const {
        float value = 0.0f;

        Vector L[5];
        if (size >= 0.5f) {
            // Handling the singularity
            L[0] = (canonicalToDirSing(p.x + size, p.y));
            L[1] = (canonicalToDirSing(p.x, p.y));
            L[2] = (canonicalToDirSing(p.x, p.y + size));
            L[3] = (canonicalToDirSing(p.x + size, p.y + size));
        }
        else {
            L[0] = (canonicalToDir(p.x + size, p.y));
            L[1] = (canonicalToDir(p.x, p.y));
            L[2] = (canonicalToDir(p.x, p.y + size));
            L[3] = (canonicalToDir(p.x + size, p.y + size));
        }

        L[0] = (invM * L[0]);
        L[1] = (invM * L[1]);
        L[2] = (invM * L[2]);
        L[3] = (invM * L[3]);

        int n = 0;
        ClipQuadToHorizon(L, n);

        if (n == 0) {
            return 0;
        }

        L[0] = normalize(L[0]);
        L[1] = normalize(L[1]);
        L[2] = normalize(L[2]);
        L[3] = normalize(L[3]);

        value += integrateQuadTreeEdgeSlow(L[0], L[1]);
        value += integrateQuadTreeEdgeSlow(L[1], L[2]);
        value += integrateQuadTreeEdgeSlow(L[2], L[3]);
        if (n >= 4) {
            L[4] = normalize(L[4]);
            value += integrateQuadTreeEdgeSlow(L[3], L[4]);
            if (n == 5)
                value += integrateQuadTreeEdgeSlow(L[4], L[0]);
        }

        return std::max(0.f, value);
    }

    inline float integrateQuadTreeQuad(const Point2f &p, float size) const {
        float value = 0.0f;

        Vector vsum(0.0f, 0.0f, 0.0f);

        // Use precomputed vector form factor 
        // the first 5 levels of diffuse materials
        if (diffuse && size >= 0.03125f) {
            int index = 0;

            // Compute index in precomputed table
            Point2f temp = p;

            int num = (1.0f/size);
            int depth = 0;

            while (num > 1) {
                num = num >> 1;
                ++depth;
            }

            int add = 4;

            for (int j = 0; j < depth - 1; ++j) {
                index += add;
                add *= 4;
            }

            temp.x *= (1.0f / size);
            temp.y *= (1.0f / size);

            index += temp.x * (1.0f / size) + temp.y;
            vsum = LUT_COSINE[index];

            vsum = transposeT * vsum;
        }
        else {
            Vector L[4];

			if (size >= 0.5f) {
				// Handling the singularity
				L[0] = (canonicalToDirSing(p.x + size, p.y));
				L[1] = (canonicalToDirSing(p.x, p.y));
				L[2] = (canonicalToDirSing(p.x, p.y + size));
				L[3] = (canonicalToDirSing(p.x + size, p.y + size));
			}
			else {
				L[0] = (canonicalToDir(p.x + size, p.y));
				L[1] = (canonicalToDir(p.x, p.y));
				L[2] = (canonicalToDir(p.x, p.y + size));
				L[3] = (canonicalToDir(p.x + size, p.y + size));
			}

            L[0] = (invM * L[0]);
            L[1] = (invM * L[1]);
            L[2] = (invM * L[2]);
            L[3] = (invM * L[3]);

            if (!diffuse) {
                L[0] = normalize(L[0]);
                L[1] = normalize(L[1]);
                L[2] = normalize(L[2]);
                L[3] = normalize(L[3]);
            }

            vsum += integrateQuadTreeEdgeVec(L[0], L[1]);
            vsum += integrateQuadTreeEdgeVec(L[1], L[2]);
            vsum += integrateQuadTreeEdgeVec(L[2], L[3]);
            vsum += integrateQuadTreeEdgeVec(L[3], L[0]);
        }

        // Find the percentage of the polygon that is above the horizon
        // using the clipped spheres approximation
        float len = vsum.length();
        float z = len == 0.0f ? 0.0f : vsum.z / len;

        int icoord = math::clamp((int)(((z + 1.0f) / 2.0f) * (LUT_CLIPPING_SIZE - 1)), 0, LUT_CLIPPING_SIZE - 1);
        int jcoord = math::clamp((int)(len * (LUT_CLIPPING_SIZE - 1)), 0, LUT_CLIPPING_SIZE - 1);

        float scale = LUT_CLIPPING[icoord + jcoord * LUT_CLIPPING_SIZE];

        value = len * scale;

        return std::max(0.0f, value);
    }
#endif // !MTS_USE_ENOKI

};

struct MultiComponentLTC {
    LTC components[2];       // Statically allocated 2 components, change based on use case
    float componentProbs[2]; // Same statically allocated
    int numComponents;

    MultiComponentLTC() {
        numComponents = 1;
    };

    MultiComponentLTC(LTC& ltc) {
        components[0] = ltc;

        numComponents = 1;
    }

#ifndef MTS_USE_ENOKI
    float integrateQuadTreeQuad(const Point2f &p, float size) const {
        float value = 0.0f;
        for (int i = 0; i < numComponents; ++i) {
            #ifdef MTS_USE_SLOW_PROD
                value += componentProbs[i]*components[i].integrateQuadTreeQuadSlow(p, size);
            #else
                value += componentProbs[i]*components[i].integrateQuadTreeQuad(p, size);
            #endif
        }

        return value;
    }
#endif

#ifdef MTS_USE_ENOKI
    inline std::array<float,4> enokiIntegrateQuadTreeQuadAll(Point2f &p, const Float &size, FloatPL &u1, FloatPL &u2) const {
        FloatP4 totalValue = FloatP4(0.f, 0.f, 0.f, 0.f);

        for (int i = 0; i < numComponents; ++i) {

            // Use precomputed vector form factor 
            // the first 5 levels of diffuse materials
            if (components[i].diffuse && size >= 0.03125f) {
                int index = 0;

                // Compute index in precomputed table
                Point2f temp = p;

                int num = (1.0f / (2.0f * size));
                int depth = 0;

                while (num > 1) {
                    num = num >> 1;
                    ++depth;
                }

                int add = 1;

                for (int j = 0; j < depth; ++j) {
                    index += add;
                    add *= 4;
                }

                index *= 12;

                temp.x *= 12 * (1.0f / (2.0f * size));
                temp.y *= 12 * (1.0f / (2.0f * size));

                index += (temp.x * (1.0f / (2.0f * size)) + temp.y);

                const uint32_t pos = enoki::clamp(index, 0, LUT_COSINE_SIZE-1);

                Vector3fP4 xyz_sum = enoki::load<Vector3fP4>(LUT_COSINE + pos);

                xyz_sum = components[i].enokiTransposeT * xyz_sum;

                FloatP4 len = enoki::norm(xyz_sum);
                FloatP4 z = xyz_sum.z() / len;


                const uint32_t size = (uint32_t)LUT_CLIPPING_SIZE,
                    size_m = size - 1;

                UInt32P4 icoord = UInt32P4((z + 1.f) * .5f * size_m),
                    jcoord = UInt32P4(len * size_m);

                icoord = enoki::clamp(icoord, 0, size_m);
                jcoord = enoki::clamp(jcoord, 0, size_m);

                FloatP4 scale = enoki::gather<FloatP4>((float*)LUT_CLIPPING, icoord + jcoord * size);

                totalValue += componentProbs[i] * len * scale;
            }
            else
            {
                const auto EPS_SING = 0.005f;
                u1[u1 < EPS_SING] = FloatPL(EPS_SING);
                u1[u1 > (1.0f - EPS_SING)] = FloatPL(1.0f - EPS_SING);
                u2[u2 < EPS_SING] = FloatPL(EPS_SING);
                u2[u2 > (1.0f - EPS_SING)] = FloatPL(1.0f - EPS_SING);

                // Canonical to directional
                const auto cosTheta = 2.f * u1 - 1.f;
                const auto phi = 2.f * Pi * u2;
                const auto sinTheta = sqrt(1.f - cosTheta * cosTheta);
                const auto eLiConst = EnokiVector3fPL(
                    sinTheta * enoki::cos(phi),
                    sinTheta * enoki::sin(phi),
                    cosTheta);

                auto eLi = components[i].enokiInvM * eLiConst;
                if(!components[i].diffuse) eLi = enoki::normalize(eLi);

                // Do the suffling for all coordinates
                EnokiVector3fPL eLj(
                        enoki::shuffle<4, 5, 6, 7,
                                8, 9, 10, 11,
                                12, 13, 14, 15,
                                0, 1, 2, 3>(eLi.x()),
                        enoki::shuffle<4, 5, 6, 7,
                                8, 9, 10, 11,
                                12, 13, 14, 15,
                                0, 1, 2, 3>(eLi.y()),
                        enoki::shuffle<4, 5, 6, 7,
                                8, 9, 10, 11,
                                12, 13, 14, 15,
                                0, 1, 2, 3>(eLi.z())
                );

                // Avoid the use of acos to compute theta by
                // using a fit of the theta over sintheta function (see [HH16])
                auto x = enoki::dot(eLi, eLj);
                auto y = enoki::abs(x);

                auto a = 0.8543985f + (0.4965155f + 0.0145206f * y) * y;
                auto b = 3.4175940f + (4.1616724f + y) * y;
                auto v = a / b;

                auto theta_sintheta = enoki::select(x > 0.0f, v, 0.5f * enoki::rsqrt(enoki::max(1.0f - x * x, 1e-7f)) - v);
                auto f = enoki::cross(eLi, eLj) * theta_sintheta;

                Vector3fP4 xyz_sum = Vector3fP4(
                        (FloatP4(_mm512_extractf32x4_ps(f.x().m, 0)) +
                            FloatP4(_mm512_extractf32x4_ps(f.x().m, 1))) +
                        (FloatP4(_mm512_extractf32x4_ps(f.x().m, 2)) +
                            FloatP4(_mm512_extractf32x4_ps(f.x().m, 3))),

                        (FloatP4(_mm512_extractf32x4_ps(f.y().m, 0)) +
                            FloatP4(_mm512_extractf32x4_ps(f.y().m, 1))) +
                        (FloatP4(_mm512_extractf32x4_ps(f.y().m, 2)) +
                            FloatP4(_mm512_extractf32x4_ps(f.y().m, 3))),

                        (FloatP4(_mm512_extractf32x4_ps(f.z().m, 0)) +
                            FloatP4(_mm512_extractf32x4_ps(f.z().m, 1))) +
                        (FloatP4(_mm512_extractf32x4_ps(f.z().m, 2)) +
                            FloatP4(_mm512_extractf32x4_ps(f.z().m, 3)))
                );

                FloatP4 len = enoki::norm(xyz_sum);
                FloatP4 z = xyz_sum.z() / len;


                const uint32_t size = (uint32_t)LUT_CLIPPING_SIZE,
                    size_m = size - 1;

                UInt32P4 icoord = UInt32P4((z + 1.f) * .5f * size_m),
                    jcoord = UInt32P4(len * size_m);

                icoord = enoki::clamp(icoord, 0, size_m);
                jcoord = enoki::clamp(jcoord, 0, size_m);

                FloatP4 scale = enoki::gather<FloatP4>((float*)LUT_CLIPPING, icoord + jcoord * size);

                totalValue += componentProbs[i] * len * scale;
            }
        }

        return {totalValue[0], totalValue[1], totalValue[2], totalValue[3]};
    }
#endif

    Vector sample(Point2f sample, Float& pdf) const {
        if(numComponents == 1) {
            auto L = components[0].sample(sample);
            pdf = components[0].eval(L);
            return L;
        }

        if(sample.x < componentProbs[0]) {
            sample.x /= componentProbs[0];
            auto L = components[0].sample(sample);
            pdf = components[0].eval(L) * componentProbs[0] + components[1].eval(L) * componentProbs[1];
            return L;
        } else {
            sample.x = (sample.x - componentProbs[0]) / (1 - componentProbs[0]);
            auto L = components[1].sample(sample);
            pdf = components[0].eval(L) * componentProbs[0] + components[1].eval(L) * componentProbs[1];
            return L;
        }
    }

    void addTransformation(Matrix3x3 &transf) {
        for (int i = 0; i < numComponents; ++i) {
            components[i].addTransformation(transf);
        }
    }

    void setTransformation(Matrix3x3 &transf) {
        for (int i = 0; i < numComponents; ++i) {
            components[i].setTransformation(transf);
        }
    }
};

MTS_NAMESPACE_END